import { ValidationPipe } from '@nestjs/common';
import { NestApplication, NestFactory } from '@nestjs/core';
// import cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import { ConfigurationService } from './configuration/configuration.service';
// import compression from 'compression';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
// import * as cookie from 'cookie-parser';

async function bootstrap() {
  // Express App
  const app = await NestFactory.create<NestApplication>(AppModule);

  // CORS Enabled
  app.enableCors();

  // App Configurations
  const config: ConfigurationService = app.get(ConfigurationService);

  // User Input Validations
  app.useGlobalPipes(new ValidationPipe());

  // Cookies Setup
  // app.use(cookie, { secret: config.cookie_secret });

  // API Response Compression in GZip format
  // app.use(compression, { encodings: ['gzip', 'deflate'] });

  // Swagger Setup
  const swaggerConfig = new DocumentBuilder()
    .setTitle(config.app.name)
    .setDescription(config.app.description)
    .setVersion(config.app.version)
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig);
  SwaggerModule.setup('docs', app, document);

  // App Initiation
  await app.listen(config.app.port);
}
bootstrap();
