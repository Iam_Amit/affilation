import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './modules/user/user.module';
import { BrandModule } from './modules/brand/brand.module';
import { ProductModule } from './modules/product/product.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { ConfigurationModule } from './configuration/configuration.module';
import { ConfigurationService } from './configuration/configuration.service';
import { CategoryModule } from './modules/category/category.module';
import { SpecificationModule } from './modules/specification/specification.module';
import { ProductSpecificationModule } from './modules/product-specification/product-specification.module';
import { LinksModule } from './modules/links/links.module';
import { CommentsModule } from './modules/comments/comments.module';
import { ReplyModule } from './modules/reply/reply.module';

@Module({
  imports: [
    ConfigurationModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigurationModule],
      useFactory: (configService: ConfigurationService) => ({
        type: 'postgres',
        host: configService.db.host,
        username: configService.db.username,
        password: configService.db.password,
        database: configService.db.name,
        port: +configService.db.port,
        entities: [__dirname + '/**/*.entity{.ts,.js}'],
        synchronize: true,
      }),
      inject: [ConfigurationService],
    }),

    UserModule,
    BrandModule,
    ProductModule,
    CategoryModule,
    SpecificationModule,
    ProductSpecificationModule,
    LinksModule,
    CommentsModule,
    ReplyModule,
  ],
  controllers: [AppController],
  providers: [AppService, ConfigurationService],
})
export class AppModule {}
