import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { CommentsService } from './comments.service';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { Response } from 'express';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService) {}

  // creating the Comments
  @Post()
  create(@Body() createCommentDto: CreateCommentDto) {
    return this.commentsService.create(createCommentDto);
  }

  // List all Comments data
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    console.log('Response = > ', res);
    const val = await this.commentsService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );
    return res.status(HttpStatus.OK).send(val);
  }

  // List the Comment Data by Id
  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.commentsService.findOne(id);
  }

  // Update the Comment Data
  @Patch(':id')
  update(@Param('id') id: number, @Body() updateCommentDto: UpdateCommentDto) {
    return this.commentsService.update(id, updateCommentDto);
  }

  // Remove the Comment
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.commentsService.remove(+id);
  }
}
