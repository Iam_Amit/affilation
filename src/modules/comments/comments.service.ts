import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CommentRepository } from './comment.repository';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';

@Injectable()
export class CommentsService {
  constructor(
    @InjectRepository(CommentRepository)
    private CommentRepo: CommentRepository,
  ) {}

  // Creating Comment API
  create(createCommentDto: CreateCommentDto) {
    return this.CommentRepo.createComment(createCommentDto);
  }

  // listing all Comment
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.CommentRepo.listComment(
      search,
      page,
      limit,
    );
    const total = await this.CommentRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get the data by Comment Id
  async findOne(id: number) {
    const found = await this.CommentRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Comment with the id ${id} not found`);
    }
    return found;
  }

  // Update the Comment Data
  async update(id: number, updateCommentDto: UpdateCommentDto) {
    return await this.CommentRepo.update(id, updateCommentDto);
  }

  // Remove the Comment Name
  async remove(id: number): Promise<void> {
    const result = await this.CommentRepo.softDelete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Comment  with the id ${id} not found`);
    }
  }
}
