import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateCommentDto } from './dto/create-comment.dto';
import { Comment } from './entities/comment.entity';

@EntityRepository(Comment)
export class CommentRepository extends Repository<Comment> {
  async createComment(createCommentDto: CreateCommentDto): Promise<Comment> {
    const { description, title } = createCommentDto;

    const comment = new Comment();

    comment.title = title;
    comment.description = description;

    try {
      await comment.save();
      delete comment.createdAt;
      delete comment.deletedAt;
      delete comment.updatedAt;
      return comment;
    } catch (error) {
      throw new InternalServerErrorException(
        'Error while saving the Comment Data!',
      );
    }
  }

  // Pagination in the Comment Data
  async listComment(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('Comment');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'comment.title LIKE :search OR Comment.description LIKE :search',
        {
          search: `%${search}%`,
        },
      );
    }
    // console.log(query.getSql());
    const comments = await query.getManyAndCount();
    // console.log('Comments => ', Comments);
    return comments;
  }
}
