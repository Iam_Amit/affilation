import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  HttpStatus,
  Res,
} from '@nestjs/common';
import { ReplyService } from './reply.service';
import { CreateReplyDto } from './dto/create-reply.dto';
import { UpdateReplyDto } from './dto/update-reply.dto';
import { Response } from 'express';

@Controller('reply')
export class ReplyController {
  constructor(private readonly replyService: ReplyService) {}

  // creating the Reply
  @Post()
  create(@Body() createReplyDto: CreateReplyDto) {
    return this.replyService.create(createReplyDto);
  }

  // List all Replys data
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    console.log('Response = > ', res);
    const val = await this.replyService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );
    return res.status(HttpStatus.OK).send(val);
  }

  // List the Reply Data by Id
  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.replyService.findOne(id);
  }

  // Update the Reply Data
  @Patch(':id')
  update(@Param('id') id: number, @Body() updateReplyDto: UpdateReplyDto) {
    return this.replyService.update(id, updateReplyDto);
  }

  // Remove the Reply
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.replyService.remove(+id);
  }
}
