import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateReplyDto } from './dto/create-reply.dto';
import { UpdateReplyDto } from './dto/update-reply.dto';
import { ReplyRepository } from './reply.repository';

@Injectable()
export class ReplyService {
  constructor(
    @InjectRepository(ReplyRepository)
    private ReplyRepo: ReplyRepository,
  ) {}

  // Creating Reply API
  create(createReplyDto: CreateReplyDto) {
    return this.ReplyRepo.createReplys(createReplyDto);
  }

  // listing all Replys
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.ReplyRepo.listReply(
      search,
      page,
      limit,
    );
    const total = await this.ReplyRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get the data by Reply Id
  async findOne(id: number) {
    const found = await this.ReplyRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Reply with the id ${id} not found`);
    }
    return found;
  }

  // Update the Reply Data
  async update(id: number, updateReplyDto: UpdateReplyDto) {
    return await this.ReplyRepo.update(id, updateReplyDto);
  }

  // Remove the Reply Name
  async remove(id: number): Promise<void> {
    const result = await this.ReplyRepo.softDelete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Reply  with the id ${id} not found`);
    }
  }
}
