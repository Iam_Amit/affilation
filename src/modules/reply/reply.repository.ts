import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateReplyDto } from './dto/create-reply.dto';
import { Reply } from './entities/reply.entity';

@EntityRepository(Reply)
export class ReplyRepository extends Repository<Reply> {
  async createReplys(createReplyDto: CreateReplyDto): Promise<Reply> {
    const { description, title } = createReplyDto;

    const reply = new Reply();

    reply.title = title;
    reply.description = description;

    try {
      await reply.save();
      delete reply.createdAt;
      delete reply.deletedAt;
      delete reply.updatedAt;
      return reply;
    } catch (error) {
      throw new InternalServerErrorException(
        'Error while saving the Reply Data!',
      );
    }
  }

  // Pagination in the Reply Data
  async listReply(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('Reply');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'Reply.description LIKE :search OR Reply.title LIKE :search',
        {
          search: `%${search}%`,
        },
      );
    }
    // console.log(query.getSql());
    const replys = await query.getManyAndCount();
    // console.log('Replys => ', Replys);
    return replys;
  }
}
