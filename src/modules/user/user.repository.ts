import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { EntityRepository, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async createUser(createUserDto: CreateUserDto): Promise<void> {
    const { userName, email, password } = createUserDto;

    const user = new User();
    user.userName = userName;
    user.email = email;
    let salt = await bcrypt.genSalt();
    user.password = await this.hashPassword(password, salt);

    try {
      await user.save();
    } catch (error) {
      console.log(error.code);
      if (error.code === '23505') {
        throw new ConflictException('Username already exist');
      } else {
        throw new InternalServerErrorException();
      }
    }
  }

  // Listing the user
  async listUser(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('users');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere('users.userName LIKE :search ', { search: `%${search}%` });
    }
    // console.log(query.getSql());
    const users = await query.getManyAndCount();
    console.log('userss => ', users);
    return users;
  }

  async hashPassword(password: string, salt: string): Promise<string> {
    return bcrypt.hash(password, salt);
  }

  //validating the user Password
  async validUserPassword(createUserDto: CreateUserDto): Promise<string> {
    const { email, password } = createUserDto;
    const user = await this.findOne({ email });
    console.log('User = > ', user);
    if (user && (await user.validatePassword(password))) {
      return user.email;
    } else {
      return null;
    }
  }
}
