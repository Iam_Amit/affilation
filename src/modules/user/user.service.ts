import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtPayload } from 'src/jwt/jwt-payload-interface';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepo: UserRepository,
    private jwtService: JwtService, // private jwtService: JwtService,
  ) {}

  //Signup User
  async create(createUserDto: CreateUserDto) {
    const createUser = await this.userRepo.createUser(createUserDto);

    return createUser;
  }

  // User Login
  async singIn(createUserDto: CreateUserDto): Promise<{ accessToken: string }> {
    const email = await this.userRepo.validUserPassword(createUserDto);
    console.log('SignIN => ', email);

    if (!email) {
      throw new UnauthorizedException('Invalid Credentials');
    }
    const payload: JwtPayload = { email };
    const accessToken = await this.jwtService.sign(payload);
    return { accessToken };
  }

  //Listing all user
  async findAll() {
    const found = await this.userRepo.findAndCount({});
    return found;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
