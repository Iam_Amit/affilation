import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { Product } from './entities/product.entity';

@EntityRepository(Product)
export class ProductRepository extends Repository<Product> {
  async createProduct(createProductDto: CreateProductDto): Promise<Product> {
    const { productName, descriptionLarge, descriptionSmall, image } =
      createProductDto;

    const product = new Product();

    product.productName = productName;
    product.descriptionSmall = descriptionSmall;
    product.descriptionLarge = descriptionLarge;
    product.image = image;

    try {
      await product.save();
      delete product.createdAt;
      delete product.deletedAt;
      delete product.updatedAt;
      return product;
    } catch (error) {
      throw new InternalServerErrorException(
        'Error while saving the product Data!',
      );
    }
  }

  // Pagination in the product Data
  async listProduct(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('product');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'product.productName LIKE :search OR product.descriptionSmall LIKE :search',
        {
          search: `%${search}%`,
        },
      );
    }
    // console.log(query.getSql());
    const products = await query.getManyAndCount();
    // console.log('products => ', products);
    return products;
  }
}
