import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { ProductRepository } from './product.repository';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(ProductRepository)
    private productRepo: ProductRepository,
  ) {}

  // create Product API
  create(createProductDto: CreateProductDto) {
    return this.productRepo.createProduct(createProductDto);
  }

  // List all the product
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.productRepo.listProduct(
      search,
      page,
      limit,
    );
    const total = await this.productRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get the data by Product Id
  async findOne(id: number) {
    const found = await this.productRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Product with the id ${id} not found`);
    }
    return found;
  }

  // Update the Product Data
  async update(id: number, updateProductDto: UpdateProductDto) {
    return await this.productRepo.update(id, updateProductDto);
  }

  // Remove the Product Name
  async remove(id: number): Promise<void> {
    const result = await this.productRepo.softDelete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Product  with the id ${id} not found`);
    }
  }
}
