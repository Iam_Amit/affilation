export class CreateProductDto {
  productName: string;

  descriptionSmall: string;

  descriptionLarge: string;

  image: string;
}
