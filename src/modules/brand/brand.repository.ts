import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateBrandDto } from './dto/create-brand.dto';
import { Brand } from './entities/brand.entity';

@EntityRepository(Brand)
export class BrandsRepository extends Repository<Brand> {
  async createBrands(createBrandDto: CreateBrandDto): Promise<Brand> {
    const { descriptionLarge, descriptionSmall, title, brandName, logo } =
      createBrandDto;

    const brand = new Brand();

    brand.brandName = brandName;
    brand.title = title;
    brand.descriptionSmall = descriptionSmall;
    brand.descriptionLarge = descriptionLarge;
    brand.logo = logo;

    try {
      await brand.save();
      delete brand.createdAt;
      delete brand.deletedAt;
      delete brand.updatedAt;
      return brand;
    } catch (error) {
      throw new InternalServerErrorException(
        'Error while saving the Brand Data!',
      );
    }
  }

  // Pagination in the brand Data
  async listBrand(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('brand');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'brand.brandName LIKE :search OR brand.title LIKE :search',
        {
          search: `%${search}%`,
        },
      );
    }
    // console.log(query.getSql());
    const brands = await query.getManyAndCount();
    // console.log('brands => ', brands);
    return brands;
  }
}
