import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BrandsRepository } from './brand.repository';
import { CreateBrandDto } from './dto/create-brand.dto';
import { UpdateBrandDto } from './dto/update-brand.dto';

@Injectable()
export class BrandService {
  constructor(
    @InjectRepository(BrandsRepository)
    private brandRepo: BrandsRepository,
  ) {}

  // Creating brand API
  create(createBrandDto: CreateBrandDto) {
    return this.brandRepo.createBrands(createBrandDto);
  }

  // listing all brands
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.brandRepo.listBrand(
      search,
      page,
      limit,
    );
    const total = await this.brandRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get the data by Brand Id
  async findOne(id: number) {
    const found = await this.brandRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Brand with the id ${id} not found`);
    }
    return found;
  }

  // Update the brand Data
  async update(id: number, updateBrandDto: UpdateBrandDto) {
    return await this.brandRepo.update(id, updateBrandDto);
  }

  // Remove the brand Name
  async remove(id: number): Promise<void> {
    const result = await this.brandRepo.softDelete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Brand  with the id ${id} not found`);
    }
  }
}
