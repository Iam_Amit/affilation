export class CreateBrandDto {
  brandName: string;

  title: string;

  descriptionSmall: string;

  descriptionLarge: string;

  logo: string;
}
