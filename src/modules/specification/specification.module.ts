import { Module } from '@nestjs/common';
import { SpecificationService } from './specification.service';
import { SpecificationController } from './specification.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SpecRepository } from './specification.repository';

@Module({
  imports: [TypeOrmModule.forFeature([SpecRepository])],
  controllers: [SpecificationController],
  providers: [SpecificationService],
  // exports: [SpecRepository],
})
export class SpecificationModule {}
