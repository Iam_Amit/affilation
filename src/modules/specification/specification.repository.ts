import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateSpecificationDto } from './dto/create-specification.dto';
import { Specification } from './entities/specification.entity';

@EntityRepository(Specification)
export class SpecRepository extends Repository<Specification> {
  async createSpecs(
    createSpecDto: CreateSpecificationDto,
  ): Promise<Specification> {
    console.log('Specification API ');
    const { name, hasOption } = createSpecDto;

    const spec = new Specification();

    spec.name = name;
    spec.hasOption = hasOption;
    try {
      await spec.save();
      delete spec.createdAt;
      delete spec.deletedAt;
      delete spec.updatedAt;
      return spec;
    } catch (error) {
      throw new InternalServerErrorException(
        'Error while saving the Specification Data!',
      );
    }
  }

  // Pagination in the Spec Data
  async listSpec(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('Spec');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere('Spec.name LIKE :search OR Spec.hasOption LIKE :search', {
        search: `%${search}%`,
      });
    }
    // console.log(query.getSql());
    const specs = await query.getManyAndCount();
    // console.log('Specs => ', specs);
    return specs;
  }
}
