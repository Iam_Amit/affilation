import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { SpecificationService } from './specification.service';
import { CreateSpecificationDto } from './dto/create-specification.dto';
import { UpdateSpecificationDto } from './dto/update-specification.dto';
import { Response } from 'express';

@Controller('specification')
export class SpecificationController {
  constructor(private readonly specificationService: SpecificationService) {}

  // creating the Specifications
  @Post()
  create(@Body() createSpecificationDto: CreateSpecificationDto) {
    return this.specificationService.create(createSpecificationDto);
  }

  // List all Specifications data
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    console.log('Response = > ', res);
    const val = await this.specificationService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );
    return res.status(HttpStatus.OK).send(val);
  }

  // List the Specification Data by Id
  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.specificationService.findOne(id);
  }

  // Update the Specification Data
  @Patch(':id')
  update(
    @Param('id') id: number,
    @Body() updateSpecificationDto: UpdateSpecificationDto,
  ) {
    return this.specificationService.update(id, updateSpecificationDto);
  }

  // Remove the Specification
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.specificationService.remove(+id);
  }
}
