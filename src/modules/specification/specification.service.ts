import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateSpecificationDto } from './dto/create-specification.dto';
import { UpdateSpecificationDto } from './dto/update-specification.dto';
import { SpecRepository } from './specification.repository';

@Injectable()
export class SpecificationService {
  constructor(
    @InjectRepository(SpecRepository)
    private specRepo: SpecRepository,
  ) {}

  // Creating spec API
  create(createSpecDto: CreateSpecificationDto) {
    return this.specRepo.create(createSpecDto);
  }

  // listing all specs
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.specRepo.listSpec(
      search,
      page,
      limit,
    );
    const total = await this.specRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get the data by spec Id
  async findOne(id: number) {
    const found = await this.specRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`spec with the id ${id} not found`);
    }
    return found;
  }

  // Update the spec Data
  async update(id: number, updateSpecDto: UpdateSpecificationDto) {
    return await this.specRepo.update(id, updateSpecDto);
  }

  // Remove the spec Name
  async remove(id: number): Promise<void> {
    const result = await this.specRepo.softDelete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`spec  with the id ${id} not found`);
    }
  }
}
