export class CreateProductSpecificationDto {
  title: string;

  descriptionSmall: string;

  descriptionLarge: string;

  value: string;
}
