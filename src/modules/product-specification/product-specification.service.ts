import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProductSpecificationDto } from './dto/create-product-specification.dto';
import { UpdateProductSpecificationDto } from './dto/update-product-specification.dto';
import { ProductSpecRepository } from './product-specification.repository';

@Injectable()
export class ProductSpecificationService {
  constructor(
    @InjectRepository(ProductSpecRepository)
    private projectSpecRepo: ProductSpecRepository,
  ) {}

  // Creating ProjectSpec API
  create(createProjectSpecDto: CreateProductSpecificationDto) {
    return this.projectSpecRepo.create(createProjectSpecDto);
  }

  // listing all ProjectSpecs
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.projectSpecRepo.listProjectSpec(
      search,
      page,
      limit,
    );
    const total = await this.projectSpecRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get the data by ProjectSpec Id
  async findOne(id: number) {
    const found = await this.projectSpecRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`ProjectSpec with the id ${id} not found`);
    }
    return found;
  }

  // Update the ProjectSpec Data
  async update(
    id: number,
    updateProjectSpecDto: UpdateProductSpecificationDto,
  ) {
    return await this.projectSpecRepo.update(id, updateProjectSpecDto);
  }

  // Remove the ProjectSpec Name
  async remove(id: number): Promise<void> {
    const result = await this.projectSpecRepo.softDelete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`ProjectSpec  with the id ${id} not found`);
    }
  }
}
