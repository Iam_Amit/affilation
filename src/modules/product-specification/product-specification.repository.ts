import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateProductSpecificationDto } from './dto/create-product-specification.dto';
import { ProductSpecification } from './entities/product-specification.entity';

@EntityRepository(ProductSpecification)
export class ProductSpecRepository extends Repository<ProductSpecification> {
  async createProjectSpecs(
    createProjectSpecDto: CreateProductSpecificationDto,
  ): Promise<ProductSpecification> {
    const { descriptionLarge, descriptionSmall, title, value } =
      createProjectSpecDto;

    const projectSpec = new ProductSpecification();

    projectSpec.value = value;
    projectSpec.title = title;
    projectSpec.descriptionSmall = descriptionSmall;
    projectSpec.descriptionLarge = descriptionLarge;

    try {
      await projectSpec.save();
      delete projectSpec.createdAt;
      delete projectSpec.deletedAt;
      delete projectSpec.updatedAt;
      return projectSpec;
    } catch (error) {
      throw new InternalServerErrorException(
        'Error while saving the projectSpec Data!',
      );
    }
  }

  // Pagination in the ProjectSpec Data
  async listProjectSpec(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('ProductSpecification');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere(
        'ProjectSpec.value LIKE :search OR ProjectSpec.title LIKE :search',
        {
          search: `%${search}%`,
        },
      );
    }
    // console.log(query.getSql());
    const ProjectSpecs = await query.getManyAndCount();
    // console.log('ProjectSpecs => ', ProjectSpecs);
    return ProjectSpecs;
  }
}
