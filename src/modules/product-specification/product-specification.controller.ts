import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { ProductSpecificationService } from './product-specification.service';
import { CreateProductSpecificationDto } from './dto/create-product-specification.dto';
import { UpdateProductSpecificationDto } from './dto/update-product-specification.dto';
import { Response } from 'express';

@Controller('productSpec')
export class ProductSpecificationController {
  constructor(
    private readonly productSpecService: ProductSpecificationService,
  ) {}

  // creating the ProductSpec
  @Post()
  create(@Body() createProductSpecDto: CreateProductSpecificationDto) {
    return this.productSpecService.create(createProductSpecDto);
  }

  // List all ProductSpec data
  @Get()
  async findAll(
    @Query('page') page,
    @Query('limit') limit,
    @Query('search') search,
    @Res() res: Response,
  ) {
    console.log('Response = > ', res);
    const val = await this.productSpecService.findAll(
      parseInt(page) - 1 || 0,
      parseInt(limit) || 10,
      search,
    );
    return res.status(HttpStatus.OK).send(val);
  }

  // List the ProductSpecData by Id
  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.productSpecService.findOne(id);
  }

  // Update the ProductSpecData
  @Patch(':id')
  update(
    @Param('id') id: number,
    @Body() updateProductSpecDto: UpdateProductSpecificationDto,
  ) {
    return this.productSpecService.update(id, updateProductSpecDto);
  }

  // Remove the ProductSpec  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.productSpecService.remove(+id);
  }
}
