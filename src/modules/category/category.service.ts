import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryReposisory } from './category.repository';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(CategoryReposisory)
    private categoryRepo: CategoryReposisory,
  ) {}

  // Creating Category API
  create(createCategoryDto: CreateCategoryDto) {
    return this.categoryRepo.createCategory(createCategoryDto);
  }

  // listing all Categorys
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.categoryRepo.listCategory(
      search,
      page,
      limit,
    );
    const total = await this.categoryRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get the data by Category Id
  async findOne(id: number) {
    const found = await this.categoryRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Category with the id ${id} not found`);
    }
    return found;
  }

  // Update the Category Data
  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    return await this.categoryRepo.update(id, updateCategoryDto);
  }

  // Remove the Category Name
  async remove(id: number): Promise<void> {
    const result = await this.categoryRepo.softDelete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Category  with the id ${id} not found`);
    }
  }
}
