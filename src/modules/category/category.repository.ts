import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { Category } from './entities/category.entity';

@EntityRepository(Category)
export class CategoryReposisory extends Repository<Category> {
  async createCategory(
    createCategoryDto: CreateCategoryDto,
  ): Promise<Category> {
    const { categoryName, image } = createCategoryDto;

    const category = new Category();

    category.categoryName = categoryName;

    category.image = image;

    try {
      await category.save();
      delete category.createdAt;
      delete category.deletedAt;
      delete category.updatedAt;
      return category;
    } catch (error) {
      throw new InternalServerErrorException(
        'Error while saving the Category Data!',
      );
    }
  }

  // Pagination in the category Data
  async listCategory(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('category');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere('category.categoryName LIKE :search ', {
        search: `%${search}%`,
      });
    }
    // console.log(query.getSql());
    const categorys = await query.getManyAndCount();
    // console.log('categorys => ', categorys);
    return categorys;
  }
}
