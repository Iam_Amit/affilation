import { Module } from '@nestjs/common';
import { CategoryService } from './category.service';
import { CategoryController } from './category.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryReposisory } from './category.repository';

@Module({
  imports: [TypeOrmModule.forFeature([CategoryReposisory])],
  controllers: [CategoryController],
  providers: [CategoryService],
})
export class CategoryModule {}
