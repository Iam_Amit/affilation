import { InternalServerErrorException } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { CreateLinkDto } from './dto/create-link.dto';
import { Link } from './entities/link.entity';

@EntityRepository(Link)
export class LinksRepository extends Repository<Link> {
  async createLink(createLinkDto: CreateLinkDto): Promise<Link> {
    const { link, productId } = createLinkDto;

    const links = new Link();

    links.link = link;
    links.productId = productId;

    try {
      await links.save();
      delete links.createdAt;
      delete links.deletedAt;
      delete links.updatedAt;
      return links;
    } catch (error) {
      throw new InternalServerErrorException(
        'Error while saving the Link Data!',
      );
    }
  }

  // Pagination in the Link Data
  async listLink(search: string, page: number, limit: number) {
    const query = this.createQueryBuilder('Link');
    if (search) {
      query.skip((page >= 0 ? page : 0) * limit);
      query.limit(limit);
      query.andWhere('link.link LIKE :search ', {
        search: `%${search}%`,
      });
    }
    // console.log(query.getSql());
    const Link = await query.getManyAndCount();
    // console.log('Link => ', Link);
    return Link;
  }
}
