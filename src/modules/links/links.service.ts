import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateLinkDto } from './dto/create-link.dto';
import { UpdateLinkDto } from './dto/update-link.dto';
import { LinksRepository } from './links.repository';

@Injectable()
export class LinksService {
  constructor(
    @InjectRepository(LinksRepository)
    private linkRepo: LinksRepository,
  ) {}

  // Creating Link API
  create(createLinkDto: CreateLinkDto) {
    return this.linkRepo.createLink(createLinkDto);
  }

  // listing all Links
  async findAll(page: number, limit: number, search: string) {
    const [output, searchTotal] = await this.linkRepo.listLink(
      search,
      page,
      limit,
    );
    const total = await this.linkRepo.count();
    const results = output.map((x) => {
      delete x.updatedAt;
      delete x.createdAt;
      delete x.deletedAt;
      return x;
    });
    return {
      results,
      searchTotal,
      total,
    };
  }

  // Get the data by Link Id
  async findOne(id: number) {
    const found = await this.linkRepo.findOne(id);
    if (!found) {
      throw new NotFoundException(`Link with the id ${id} not found`);
    }
    return found;
  }

  // Update the Link Data
  async update(id: number, updateLinkDto: UpdateLinkDto) {
    return await this.linkRepo.update(id, updateLinkDto);
  }

  // Remove the Link Name
  async remove(id: number): Promise<void> {
    const result = await this.linkRepo.softDelete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Link  with the id ${id} not found`);
    }
  }
}
