import { registerAs } from '@nestjs/config';

export default registerAs('appConfig', () => ({
  app: {
    port: process.env.APP_PORT,
    name: process.env.APP_NAME,
    description: process.env.APP_DESCRIPTION,
    version: process.env.APP_VERSION,
  },
  env: process.env.ENVIRONMENT,
  db: {
    host: process.env.DB_HOST,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    name: process.env.DB_NAME,
  },
  cookie_secret: process.env.COOKIE_SECRET
}));
