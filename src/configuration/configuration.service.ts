import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class ConfigurationService {
  constructor(private configService: ConfigService) {}

  get db() {
    let info = {
      name: this.configService.get<string>('appConfig.db.name'),
      host: this.configService.get<string>('appConfig.db.host'),
      username: this.configService.get<string>('appConfig.db.username'),
      password: this.configService.get<string>('appConfig.db.password'),
      port: this.configService.get<number>('appConfig.db.port'),
    };
    return info;
  }

  get app() {
    let info = {
      name: this.configService.get<string>('appConfig.app.name'),
      port: this.configService.get<string>('appConfig.app.port'),
      description: this.configService.get<string>('appConfig.app.description'),
      version: this.configService.get<string>('appConfig.app.version'),
    };
    return info;
  }

  get env(): string {
    return this.configService.get<string>('appConfig.env');
  }
  
  get cookie_secret(): string {
    return this.configService.get<string>('appConfig.cookie_secret');
  }
}
