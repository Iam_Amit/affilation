# Base App

A Starter Backend Project for all Upcoming Projects. All Base Functionalities have been implemented as Listed Below

- Configurations
  - Express
  - TypeORM
  - PostgreSQL
  - Enviorment
  - Cookies
  - HBS
  - Swagger
- Modules
  - User Module (only basic columns and crud with login and signup)
  - Authentication
    - Local
    - Google
    - Facebook
    - Apple
  - Websocket

## Technique/Configuration Implemented

---

[NestJS](https://nestjs.com/) for building efficient, reliable and scalable server-side applications.

[Express](https://expressjs.com/) Default api support

[Type ORM](https://typeorm.io/) for Object–relational mapping.

[PostgreSQL](https://www.postgresql.org/) for database,An open source object-relational database system.

[Configuration](https://docs.nestjs.com/techniques/configuration) for reading and managing data from .env .

[Cookies](https://github.com/expressjs/cookie-parser#readme) to add support for reading and setting cookies.

[Compress](https://github.com/expressjs/compression) to add compression utils to api response.

[HBS](https://handlebarsjs.com/) for minimal templating on steroids :) .

[Swagger](https://swagger.io/) for API Documentation.

[Compodoc](https://compodoc.app/) for Poject Documentation / Knowledge Transfer.

[Passport](http://www.passportjs.org/) for User Authentication with Multiple Strategies.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run dev

# production mode
$ npm run deploy
```

## Stay in touch

---

- Author - [Mohit Bhagat](https://github.com/mk8054)

---
